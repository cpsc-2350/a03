import { Named, Valued } from "./util";

export interface FoodItem extends Named, Valued {
  readonly cals: number;
}
export class FoodItemImpl implements FoodItem {
  readonly value: number;

  constructor(
    readonly name: string,
    readonly price: number,
    readonly cals: number
  ) {
    this.name = name;
    this.price = price;
    this.cals = cals;
    this.value = cals / price;
  }

  isBetterValue(o: Valued): boolean;
  isBetterValue(o: number): boolean;
  isBetterValue(o: any): any {
    if (typeof o === "number") {
      return this.value > o;
    } else {
      return this.value > o.value;
    }
  }
}

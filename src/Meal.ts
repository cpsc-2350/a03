import { Valued } from "./util";
import { FoodItem } from "./FoodItem";

export interface Meal extends Valued {
  readonly cals: number;
  add(food: FoodItem): Meal;
}

export class MealImpl implements Meal {
  private readonly foods: FoodItem[] = [];
  private sumCals: number = 0;
  private sumPrice: number = 0;

  constructor() {}

  isBetterValue(o: Valued): boolean;
  isBetterValue(o: number): boolean;
  isBetterValue(o: any): any {
    if (typeof o === "number") {
      return this.value > o;
    } else {
      return this.value > o.value;
    }
  }

  add(food: FoodItem): MealImpl {
    this.foods.push(food);
    return this;
  }

  get cals(): number {
    this.sumCals = 0;
    this.foods.forEach((item) => {
      this.sumCals += item.cals;
    });
    return this.sumCals;
  }

  get price(): number {
    this.sumPrice = 0;
    this.foods.forEach((item) => {
      this.sumPrice += item.price;
    });
    return this.sumPrice;
  }

  get value(): number {
    return this.cals / this.price;
  }
}

import { FoodItemImpl } from "./FoodItem";
import { MealImpl } from "./Meal";
import { Valued } from "./util";

const food1 = new FoodItemImpl("butter bagels", 2.35, 120);
const food2 = new FoodItemImpl("chocolate croissant", 1.49, 145);
const food3 = new FoodItemImpl("black coffee", 1.15, 110);

const food4 = new FoodItemImpl("cheese burger", 3.75, 160);
const food5 = new FoodItemImpl("donut", 1.49, 145);
const food6 = new FoodItemImpl("coke", 0.45, 92);

const meal1 = new MealImpl();
meal1.add(food1);
meal1.add(food2);
meal1.add(food3);

const meal2 = new MealImpl();
meal2.add(food4);
meal2.add(food5);
meal2.add(food6);

const food0 = new FoodItemImpl("diet coke", 0.55, 0);

function getBestValue(...meals: Valued[]): Valued {
  let betterValued: Valued = meals[0];
  for (let i = 1; i < meals.length; i++) {
    if (!meals[i].isBetterValue(betterValued.value)) {
      betterValued = meals[i];
    }
  }
  return betterValued;
}

console.log(getBestValue(meal1, meal2, food0));

export interface Named {
  readonly name: string;
}

export interface Priced {
  readonly price: number;
}

export interface Valued extends Priced {
  readonly value: number;
  isBetterValue(o: Valued): boolean;
  isBetterValue(o: number): boolean;
}
